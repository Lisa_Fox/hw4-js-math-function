//ТЕОРЕТИЧНІ ПИТАННЯ
//1. Функції — це головні “будівельні блоки” програми. Вони дозволяють робити однакові дії багато разів без повторення коду.
//2. Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції).
// Ми оголошуємо функції, вказуючи їхні параметри, потім викликаємо їх, передаючи аргументи.
// Якщо викликати функцію без аргументів, тоді відповідні значення стануть undefined.
//3. Return — інструкція мов програмування призначена для повернення з підпрограми (функції, методу, процедури) 
//в точку після коду програми, де відбувся виклик даної підпрограми.

let firstNumber;
let mathOperation;
let secondNumber;

while (firstNumber<0 || secondNumber<0 || !firstNumber || !secondNumber || !isFinite(firstNumber) || !isFinite(secondNumber)){
    firstNumber = +prompt("Enter your first number");
    mathOperation = prompt("Enter your math operation", "+, -, *, /");
    secondNumber = +prompt("Enter your second number");
}

function result(a, c, b) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    default:
      return "Error";
  }
}
console.log(result(firstNumber, mathOperation, secondNumber));
